/*
	First, we load the expressjs module into our application and saved it in a variable called express.
*/
const express = require("express");
/*
	Create an application with expressjs
	This creates an application that uses express and stores in a variable called app. Which makes it easier to use expressjs methods in our api.
*/
const app = express();
/*
	port is just a variable to contain the port number we want to designate for our new expressjs api.
*/
const port = 4000;

/*
	To create a new route in expressjs, we first access from our express() package, our method. For get method request, access express (app), and use get()

	syntax:

	app.routemethod('/',(req,res)=>{
	
		//function to handle request and response

	})

*/

// Middleware
// Will allow your app to read a JSON data
// Setup for allowing the server to handle data from requests(client)
app.use(express.json())

app.get('/',(req,res)=>{

	//res.send() ends the response and sends your response to the client.
	//res.send() already does the res.writeHead() and res.end() automatically.
	res.send("Hello World from our New ExpressJS api!");

})

app.get('/Hello', (req, res) => {
	res.send("Hello World from our New ExpressJS api!")
	//console.log("Server is running")
})

app.post('/Hello', (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

// app.post('/Hello', (req, res) => {
// deconstruct object from request body
// const {name, age, address} = req.body 
// 	res.send(`Hello there i'm ${req.body.firstName} ${req.body.lastName}, ${req.body.age} years old. `)
// })

// Mock Data
let users = []

app.post('/signup', (req, res) => {
	console.log(req.body)

	if(req.body.username != '' && req.body.password != '') {
		users.push(req.body)

		res.send(`User ${req.body.username} successfully reguistered.`)
	} else{
		res.send(`Please input BOTH username and password`)
	}
})


//PUT Route
app.put("/change-password", (req, res) => {
	let message

	for(let i = 0; i < users.length; i++) {
		if(req.body.username === users[i].username) {
			users[i].password = req.body.password
			message = `User ${req.body.username}'s password has been updated.`
			break
		} else {
			message = `User does not exist`
		}
	}

	res.send(message)
})

// ACTIVITY =====================================================
// 1. GET /home
app.get('/home', (req, res) => {
	res.send(`Welcome home!`)
})

// 3. GET users
app.get('/users', (req, res) => {
	res.send(users)
})

// 5. DELETE /delete-user
app.delete('/delete-user', (req, res) => {
	let message

	for(let i=0; i < users.length; i++) {
		if(req.body.username === users[i].username){
		console.log(users[i])

		index = users.indexOf(users[i])
		users.splice(index, 1)

		message = `User ${users[i].username} has been deleted.`
		break
		} else {
			message = `User does not exist`
		}
	}
	res.send(users)

	// // const foundItem = users.find(e => e.username === req.username)
	// // users.pop()
	// user = JSON.stringify(req.body.username)
	// removeUser = (name, value) => {
	// 	let array = $.map(this, function(v,i) {
	// 		return v[name] === value ? null : v
	// 	})
	// 	this.length = 0
	// 	this.push.apply(this, array)
	// }
	// console.log(user)
	// removeUser('username', username)
	// res.send(user)
})





/*
	app.listen() allows us to designate the correct port to our new expressjs api and once the server is running we can then run a function that runs a console.log() which contains a message that the server is running.

*/
app.listen(port,()=>console.log(`Server is running at port ${port}`));